<p align="center">
<a>NetScan</a>
</p>

## A propos de NetScan

NetScan est une application web de lecture de scan. Il possede des fonctionalité payante, qui donne accès au utilisateur Prenium d'accéder a un large biblioteque de serie.
Les users gratuit on le droit de lire que un certain nombre de serie.
Il est possible pour les utilisateurs d'uploader des scans qui passeront ensuite par une verification par les admins.


## Installation

Pour installer ce project en local il faudra créer une base "netscan"
Puis lancer la cmd "php artisan migrate:install"
Puis "php artisan migrate"
puis "php artisan db:seed"

## Les problème rencontrer 

Prinda SIVAKUMARAN :
Affichage des chapitre dans la pages des ChapterPage
Request : BookController

Maxime Domenjoud :
Probleme des lien dans la base de donnée 
Manque de temps pour faire un beau css
Probleme dans le creation des user et des check admins
Beaucoup de correction de bug lors des merge sur le git
Probleme avec les adresse des images
Mauvaise gestion du temps 


## Les taches effectuer

Maxime Domenjoud :  - creation des base, des seeds, creation des liens entre base 
                    - creation navbar 
                    - affichage des image et des pdf dans le site 
                    - correction de bug 
                    - gestion du git et des pull request 
                    - commencement du css

Prinda SIVAKUMARAN : - creation des controller (BookController - Chapter Controller)
                     - creation des model (Book - Category -Chapter)
                     - creation des fichier views 
                     - creation du fichier user( fichier auth )
