<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = "chap";

    protected $fillable = ['name', 'dateSortie', 'volume', 'image', 'series_id'];
    /**
     * Get book of chapter
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function book()
    {
        return $this->hasOne('App\Book', 'id', 'series_id');
    }

}
