<?php

namespace App\Http\Controllers;

use App\Book;
use App\Chapter;
use App\Http\Requests\storePostsRequest;
use Illuminate\Http\Request;
use Psy\Util\Str;

class ChapterController extends Controller
{
    /**
     * View of home with all chapters
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function chapterBook($name)
    {

        $chapters = Chapter::all();
        $book = Book::where('name', $name)->firstOrFail();
        return view('detailsBook')
            ->with('chapters', $chapters)
            ->with('book',$book);
    }

    public function chapter($chapter)
    {
        $chapterName = Chapter::where('chapter',$chapter)->orWhere('name', $book)->firstOrFail();

        return view('chapterPage')
            ->with('chapters', $chapterName)
            ->with('books',$books);
    }


}
