<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\Chapter;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * View of home with all books
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //Permet de récupérer tous les articles de la base de données
        $books = Book::all();

        //Retourne la vue home avec les posts
        return view('home')->with('books', $books);
    }

    /**
     * Details of book
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($slug)
    {
        $book= Book::where('name', $slug)->firstOrFail();
        $categories = Category::all();
        $chapters = Chapter::all();
        return view('detailsBook')
            ->with('book', $book)
            ->with('categories', $categories)
            ->with('chapters',$chapters);
    }

    public function chapter($book,$chapter)
    {
        $chapterName = Chapter::where('name',$chapter)->orWhere('series_id', $book)->firstOrFail();
        $bookName = Book::where('name',$book)->firstOrFail();
        $chapters = Chapter::all();
        return view('chapterPage')
            ->with('chapters',$chapters)
            ->with('chapter', $chapterName)
            ->with('book',$bookName);
    }

}
