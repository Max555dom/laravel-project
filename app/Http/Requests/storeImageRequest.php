<?php

namespace App\Http\Requests;

use Faker\Provider\Image;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class storePostsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public static function updateImage(Request $request,$id,$nom_image)
    {

        $image  = $request->file('file');
        $fileName = time() . '.' . $image->getClientOriginalExtension();

        $img = Image::make($image->getRealPath());
        $img->resize(200, 200);
        $name = $id.' '.$image->getClientOriginalName();
        $img->save('img/'.$name);



        //Enregistrement dans la bdd
        User::where('id', $id)->update(array('nom_image' => $name));
        return true ;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3'
        ];
    }
}
