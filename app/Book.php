<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = "series";

    protected $fillable = ['name', 'dateSortie', 'category_id', 'image', 'user_id'];

    /**
     * Get category of book
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    /**
     * Get all chapter with the same book
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function chapters()
    {
        return $this->hasMany('App\Chapter','series_id', 'id');
    }
}
