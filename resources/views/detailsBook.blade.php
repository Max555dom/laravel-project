@extends('layouts.base')
@section('title') Details livre @endsection

@section('content')
<div>
    <div class="container card">
        <div class="card-body">
            <div class="container">
            <h1>{{$book->name}}</h1>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-4">
                        <label>Genre : </label>
                    </div>
                    <div class="col-8">
                        <p></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <label>Catégorie : </label>
                    </div>
                    <div class="col-8">
                        <p>{{$book->category->name}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <p>Date de parution : </p>
                    </div>
                    <div class="col-8">
                        <p>{{$book->dateSortie}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <p>Note : </p>
                    </div>
                    <div class="col-8">
                        <p>{{$book->note}}</p>
                    </div>
                </div>
            </div>
            <div class="container">
                @include('chapter')
            </div>
        </div>
    </div>

</div>
@endsection
