@extends('layouts.base')


@section('title') Accueil @endsection

@section('content')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <div class="container">
        <div class="row">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @foreach($books as $book)
                    <div class="carousel-item active">
                        <rect>
                        <img src="{{asset('storage/'.$book->image)}}" class="d-block w-100" href="{{route('detailBook', $book->name)}}" height="300">
                        </rect>
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class=" card col-10">
                @foreach($books as $book)
                    <div class="col-md-10" >
                        <div class="card" >
                            <div class="card-body" display="flexbox">
                                <div class="row">
                                    <div class="col-9">

                                        <h5 class="card-title">{{$book->name}}</h5>
                                        <p>{{$book->dateSortie}}</p>
                                        <p>{{$book->category->name}}</p>
                                    </div>

                                    <div class="col-8">
                                        <img src="{{asset('storage/'.$book->image)}}" height="200" alt="#">
                                    </div>
                                </div>
                                <a id="{{$book->id}}" href="{{route('detailBook', $book->name)}}" class="btn btn-primary">Accéder à la série</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-2">

            </div>
        </div>
    </div>
@endsection
