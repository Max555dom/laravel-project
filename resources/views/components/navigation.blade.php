<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">NetScan</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{route('home')}}">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Les mangas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Ajouter un manga</a>
            </li>

            <li class=" nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Compte
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @if(\Illuminate\Support\Facades\Auth::check())
                        <a class="nav-link" href="#">{{\Illuminate\Support\Facades\Auth::user()->name}}</a>
                        <div class="dropdown-divider"></div>
                        <a class="nav-link" href="{{url('/logout')}}">Déconnexion</a>
                    @else
                        <a class="nav-link" href="{{route('login')}}">Connexion</a>
                        <div class="dropdown-divider"></div>
                        <a class="nav-link" href="{{route('register')}}">Inscription</a>
                    @endif
                </div>
            </li>

        </ul>
    </div>
</nav>
