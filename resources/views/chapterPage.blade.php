@extends('layouts.base')

@section('title') Chapitre @endsection

@section('content')
        <h1 class="text-center">{{$book->name}} {{$chapter->name}}</h1>
    <div class="container">
        <div>
            <div class="">
                <label>Chapitre : </label>
                <select type="text" name="chapter" class="form-control" value="">
                    @foreach($chapters as $chap)
                        @if($book->id == $chap->series_id)
                            <option value="{{$chap->id}}">{{$chap->name}}</option>
                        @endif
                    @endforeach
                </select>

            </div>
        </div>

    <div>
        <embed src="{{asset('storage/'.$chapter->image)}}" width=800 height=500 type='application/pdf' class="container-fluid"/>
    </div>
</div>
@endsection
