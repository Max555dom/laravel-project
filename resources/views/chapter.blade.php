<div>
    <ul>
    @foreach($chapters as $chapter)
        @if($book->id == $chapter->series_id)
            <li><a href="{{route('chapter',[$book->name,$chapter->name])}}">{{$chapter->name}}</a></li>
        @endif
    @endforeach
    </ul>
</div>
