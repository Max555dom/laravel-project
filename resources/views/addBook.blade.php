@extends('layouts.base')
<div class="">
    <form method="post" action="">
        @csrf
        <div class="form-group">
            <label>Série</label>
            <select type="text" name="book" class="form-control" required value="">
                @foreach($books as $book)
                    <option value="{{$book->id}}">{{$book->name}}</option>
                @endforeach
            </select>
            <label>Tome</label>
            <select type="text" name="$chapter" class="form-control" value="">
                @foreach($chapter as $chapter)
                    <option value="{{$chapter->id}}">{{$chapter->volume}}</option>
                @endforeach
            </select>
            <div>
                <input type="file" name="upload" class="form-control" accept=".pdf">
                <button type="submit">
            </div>
        </div>

        <button type="submit" class="btn btn-primary" >Envoyer le Scan</button>

        @include('components.errors')
    </form>
</div>
