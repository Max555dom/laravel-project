<?php

use Illuminate\Database\Seeder;

class chapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chap')->insert([
            'name' => 'Chap 1',
            'id'=> '1',
            'volume'=>'1',
            'dateSortie'=>'1997-07-22',
            'image'=>'chapitre/OnePiece/Volume1.pdf',
            'series_id'=>'1',

        ]);
        DB::table('chap')->insert([
            'name' => 'Chap 1 : Cruauté',
            'id'=> '2',
            'volume'=>'1',
            'dateSortie'=>'1997-07-22',
            'image'=>'chapitre/KimetsuNoYaiba/Cruauté.pdf',
            'series_id'=>'2',

        ]);
    }
}
