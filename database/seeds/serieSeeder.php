<?php

use Illuminate\Database\Seeder;

class serieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('series')->insert([
            'id'=> '1',
            'name' => 'One Piece',
            'auteur'=>'Eiichiro Oda',
            'dateSortie'=>'1997-07-22',
            'category_id'=>'1',
            'isPrenium'=>'1',
            'image'=>'serie/onePiece.png',
            'note'=>'5',
        ]);
        DB::table('series')->insert([
            'id'=> '2',
            'name' => 'Kimetsu No Yaiba',
            'auteur'=>'Koyoharu Gotōge',
            'dateSortie'=>'2016-02-15',
            'category_id'=>'1',
            'isPrenium'=>'0',
            'image'=>'serie/kimetsuNoYaiba.png',
            'note'=>'5',
        ]);
        DB::table('series')->insert([
            'id'=> '3',
            'name' => 'Sun Ken Rock',
            'auteur'=>'Boichi',
            'dateSortie'=>'2006-04-24',
            'category_id'=>'2',
            'isPrenium'=>'0',
            'image'=>'serie/sunKenRock.png',
            'note'=>'5',
        ]);
    }
}
