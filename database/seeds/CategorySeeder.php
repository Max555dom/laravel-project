<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Shonen',
            'id'=> '1',

        ]);
        DB::table('categories')->insert([
            'name' => 'Seinen',
            'id'=> '2',

        ]);
    }
}
