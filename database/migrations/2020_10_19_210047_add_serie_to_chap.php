<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSerieToChap extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chap', function (Blueprint $table) {
            $table->unsignedBigInteger('series_id')->nullable();
            $table->foreign('series_id')
                ->references('id')
                ->on('series')
                ->onDelete('SET NULL');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chap', function (Blueprint $table) {
            //
        });
    }
}
